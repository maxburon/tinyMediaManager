package org.tinymediamanager.core.threading;

public interface TmmTaskListener {
  void processTaskEvent(TmmTaskHandle task);
}
